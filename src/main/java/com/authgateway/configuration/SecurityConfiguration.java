package com.authgateway.configuration;

import com.authgateway.model.CredentialsProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

import java.util.List;
import java.util.stream.Collectors;

@EnableWebFluxSecurity
@Configuration
public class SecurityConfiguration {

    @Autowired
    private CredentialsProperties credentialsProperties;

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity) {
        return httpSecurity.authorizeExchange()
                .pathMatchers("/actuator/**").permitAll()
                .anyExchange().authenticated()
                .and().httpBasic()
                .and().csrf().disable()
                .build();
    }

    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        List<UserDetails> userDetails = credentialsProperties.getCredentials()
                .stream()
                .map(credentials -> User.withUsername(credentials.getUsername())
                        .password(encoder().encode(credentials.getPassword()))
                        .roles("USER")
                        .build())
                .collect(Collectors.toList());
        return new MapReactiveUserDetailsService(userDetails);
    }
}
