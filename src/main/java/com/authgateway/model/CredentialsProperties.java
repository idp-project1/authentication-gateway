package com.authgateway.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "authorization")
public class CredentialsProperties {

    private List<Credentials> credentials;

    public void setCredentials(List<Credentials> credentials) {
        this.credentials = credentials;
    }

    public List<Credentials> getCredentials() {
        return credentials;
    }
}