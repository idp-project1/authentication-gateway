package com.authgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthGateway {

    public static void main(String[] args) {
        SpringApplication.run(AuthGateway.class, args);
    }

}
