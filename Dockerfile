FROM openjdk:11-jdk-slim
VOLUME /tmp
ADD *.jar authentication-gateway.jar
ARG ACTIVE_PROFILE
ARG JAVA_OPTS
ENV ACTIVE_PROFILE=${ACTIVE_PROFILE}
ENV JAVA_OPTS=${JAVA_OPTS}

EXPOSE 8085

ENTRYPOINT java ${JAVA_OPTS} -Dserver.port=8085 -Dreactor.netty.http.server.accessLogEnabled=true \
                            -Dspring.profiles.active=${ACTIVE_PROFILE:-local} -Djava.security.egd=file:/dev/./urandom \
                            -jar /authentication-gateway.jar
