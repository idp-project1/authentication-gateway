# Authentication and forwarding unit for business logic

[![build status](https://gitlab.com/idp-project1/authentication-gateway/badges/master/pipeline.svg)](https://gitlab.com/idp-project1/authentication-gateway/commits/master)

(C) Copyright 2021

The team:

- Călătoaie Iulia-Adriana 343 C1

- Georgescu Alin-Andrei 342 C3

- Millio Anca 342 C4

- Negru Bogdan-Cristian 342 C3

This Maven generated component uses Basic Auth in controlling traffic to a
destination.
